package main

import (
	"fmt"
	"math"      // does not bring in the sub packages
	"math/rand" // sub package insude an package is called by /, and has to be called here and not in the fucntion
)

// the capital lettered functions are the ones that are imported

// only main has to run by default
// positioning of the functions does not matter

func foo() {
	fmt.Println("Square root of four is", math.Sqrt(4))
	fmt.Println("A random number is ", rand.Intn(100)) //random number generator (generates 81 everytime)

}

func main() {
	foo()
}

// to know more about a function, in cmd, godoc <package name> fucntion
// ex: godoc math/rand Intn

/*

Square root of four is 2
A random number is  81

*/
