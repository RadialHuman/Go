package main

import (
	"fmt"
	"net/http" // std library
)

func home_page_handeller(w http.ResponseWriter, r *http.Request) { // this is how others write, custom type, more on this later.
	fmt.Fprintf(w, "This is it!") // this is the output in the browser
}

func about_handeller(w http.ResponseWriter, r *http.Request) { // this si how others write
	fmt.Fprintf(w, "This is a go web app output") // this is the output in the browser
}

func main() {
	// handellers  : in web dev, there should be an url which has a path, this path has tp be mapped to functions
	http.HandleFunc("/", home_page_handeller) // home page "/" , function associated to it
	// new page
	http.HandleFunc("/about/", about_handeller)
	// servers are also required
	http.ListenAndServe(":8000", nil) // port and server parameters. Nil is like NaN
}

/*



 */
