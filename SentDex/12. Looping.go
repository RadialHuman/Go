package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
)

// there is no while loop like classes in go
// only for loop exists

type Sitemap struct {
	Locations []Location `xml:"sitemap"`
}

type Location struct {
	Loc string `xml:"loc"`
}

func (l Location) String() string {
	return fmt.Sprintf(l.Loc)
}

func main() {

	// basic for loop but not used in practice
	for i := 0; i < 10; i++ { // := is just for initialization and not for reassigning , thats just =
		fmt.Println(i)
	}

	// same thing in a more practical way
	i := 0
	for i < 10 {
		fmt.Println(i)
		i += 1
	}

	/*
		infinit loop
		for {
			fmt.Println("something")
		}
	*/

	x := 5
	for {
		fmt.Println(x) // infinite loop
		x += 3         // increment
		if x > 25 {    // to stop the loop a condition
			break // to stop the loop
		}
	}

	/* also can be written as
	for x := 5; x < 25; x += 3 {
		fmt.Println(x)
	}
	*/

	// using loop for the xml parsed data
	resp, _ := http.Get("https://www.washingtonpost.com/news-sitemap-index.xml")
	bytes, _ := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	var s Sitemap
	xml.Unmarshal(bytes, &s)

	for i, Location := range s.Locations { // rnage returns index and value
		fmt.Printf("%d\n%s\n", i, Location)
	}
}

/*
0
1
2
3
4
5
6
7
8
9
0
1
2
3
4
5
6
7
8
9
5
8
11
14
17
20
23
0
http://www.washingtonpost.com/news-politics-sitemap.xml
1
http://www.washingtonpost.com/news-blogs-politics-sitemap.xml
2
http://www.washingtonpost.com/news-opinions-sitemap.xml
3
http://www.washingtonpost.com/news-blogs-opinions-sitemap.xml
4
http://www.washingtonpost.com/news-local-sitemap.xml
5
http://www.washingtonpost.com/news-blogs-local-sitemap.xml
6
http://www.washingtonpost.com/news-sports-sitemap.xml
7
http://www.washingtonpost.com/news-blogs-sports-sitemap.xml
8
http://www.washingtonpost.com/news-national-sitemap.xml
9
http://www.washingtonpost.com/news-blogs-national-sitemap.xml
10
http://www.washingtonpost.com/news-world-sitemap.xml
11
http://www.washingtonpost.com/news-blogs-world-sitemap.xml
12
http://www.washingtonpost.com/news-business-sitemap.xml
13
http://www.washingtonpost.com/news-blogs-business-sitemap.xml
14
http://www.washingtonpost.com/news-technology-sitemap.xml
15
http://www.washingtonpost.com/news-blogs-technology-sitemap.xml
16
http://www.washingtonpost.com/news-lifestyle-sitemap.xml
17
http://www.washingtonpost.com/news-blogs-lifestyle-sitemap.xml
18
http://www.washingtonpost.com/news-entertainment-sitemap.xml
19
http://www.washingtonpost.com/news-blogs-entertainment-sitemap.xml
20
http://www.washingtonpost.com/news-blogs-goingoutguide-sitemap.xml
21
http://www.washingtonpost.com/news-goingoutguide-sitemap.xml
*/
