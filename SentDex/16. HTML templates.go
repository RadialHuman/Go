package main

import (
	"fmt"
	"html/template"
	"net/http"
)

// a variable has to be passed to the news aggregator page which will need a struct

type NewsAggPage struct {
	Title string
	News  string
}

func HomePageHandelle(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>This is it!</h1>")
}

func NewsAggHandeller(w http.ResponseWriter, r *http.Request) {
	p := NewsAggPage{Title: "News title 1", News: "News body 1"} //  values in the page
	// template calling
	t, _ := template.ParseFiles("basictemplating(HTML templates).html")
	// executing the page
	t.Execute(w, p) // writer and page
}

func main() {
	http.HandleFunc("/", HomePageHandelle)
	// news aggregator page
	http.HandleFunc("/agg/", NewsAggHandeller) // this will use and execute the template
	http.ListenAndServe(":8000", nil)
}

/*

News title 1
News body 1


*/
