package main

import (
	"fmt"
)

// there are methods in go
// two types : value receivers and pointers receivers
// value methods: calculation on values
// pointers methods: modification some part of struct

const uSixteenBitMax float64 = 65535
const kmh_multiple float64 = 1.60934 // converting miles to km

type car struct {
	gas_pedal      uint16
	break_pedal    uint16
	steering_wheel int16
	top_speed_kmph float64
}

// value receiver method which calculates kmph, which is associated to the struct above
func (c car) kmh() float64 { // c car is for assosiation and kmh is the method name
	return float64(c.gas_pedal) * (c.top_speed_kmph / uSixteenBitMax) // just a calculation
}

func (c car) mph() float64 { // c car is for assosiation and kmh is the method name
	return float64(c.gas_pedal) * (c.top_speed_kmph / uSixteenBitMax / kmh_multiple) // just a calculation
}

func main() {
	// var a_car car, is an option, but
	a_car := car{gas_pedal: 123,
		break_pedal:    0,
		steering_wheel: 12345,
		top_speed_kmph: 300.0}

	// a_car := car{123,0,12345,300.0} can also be done
	fmt.Println(a_car.gas_pedal)
	fmt.Println(a_car.kmh())
	fmt.Println(a_car.mph())
}

/*

123
0.563057907988098
0.34986883317887957


*/
