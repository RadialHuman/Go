package main

import (
	"fmt"
	"net/http"
)

func home_page_handeller(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Something</h1>") // html tags can be passed, to see thechange from normal to h1, re run in cmd and refresh
	fmt.Fprintf(w, "<p>Something else</p>")
	fmt.Fprintf(w, "<p>Something more</p>")
	fmt.Fprintf(w, "This %s an example for %s insertion", "is", "<strong>string</strong>")

	// all this when viewd in source, occurs in one line

	fmt.Fprintf(w, `<h1>Something</h1>
<p>Something else</p>
<p>Something more</p>
		`)
}

// This si difficult to deal with so there are html templates avialable, shown later

func main() {
	http.HandleFunc("/", home_page_handeller)
	http.ListenAndServe(":8000", nil)

}

/*

<h1>Something</h1><p>Something else</p><p>Something more</p>This is an example for <strong>string</strong> insertion<h1>Something</h1>
<p>Something else</p>
<p>Something more</p>



*/
