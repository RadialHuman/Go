package main

import (
	"fmt"
)

// channels are to be used to send and receieve value between goRoutines
// unsgin channel operator "<-"

func foo(c chan int, someValue int) { // pass channel along with variable for function
	// to pass the value to channel
	c <- someValue * 5
	// no return since passed through channel

}

func main() {
	// channel making
	fooVal := make(chan int) //  int can be replaced by a struct

	go foo(fooVal, 5)
	go foo(fooVal, 3)
	go foo(fooVal, 2)

	// receiving the values
	v1, v2, v3 := <-fooVal, <-fooVal, <-fooVal

	fmt.Println(v1, v2, v3)

	// this is not synced, there is buffering over channel and iterate over channel
}

/*

25 15 10

*/
