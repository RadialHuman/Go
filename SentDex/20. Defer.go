package main

import (
	"fmt"
	"sync"
	"time"
)

// Defer : if error occurs before close comes into action, the rest fo the operation will not get executed
// it will be evaluated when it gets hit, but the defered operation does not happen till the surrounding function is over or panics out
// defer can handel if there is an error by checking the panic and recover by a set of operations

var wg sync.WaitGroup

func say(s string) {
	defer wg.Done()
	for i := 0; i < 3; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}

func main() {
	wg.Add(1)
	go say("Hey")
	wg.Add(1)
	go say("there")
	wg.Wait()
}

/*

Hey
there
there
Hey
Hey
there


*/
