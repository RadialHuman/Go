package main

// combining the web app making with the news extractor codes
// using javascript template datatable to mak ethings look better
// need : jquery, datatable css, datatable js using cdn version

import (
	"encoding/xml"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
)

type NewsMap struct {
	Keyword  string
	Location string
}

type Sitemap struct {
	Locations []string `xml:"sitemap>loc"`
}

type News struct {
	Titles    []string `xml:"url>news>title"`
	Keywords  []string `xml:"url>news>keywords"`
	Locations []string `xml:"url>loc"`
}

type NewsAggPage struct {
	Title string
	News  map[string]NewsMap
}

func HomePageHandeller(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>This is it!</h1>")
}

func NewsAggHandeller(w http.ResponseWriter, r *http.Request) {

	var s Sitemap
	var n News
	// initiating the map (dictionary)
	news_map := make(map[string]NewsMap)

	resp, _ := http.Get("https://www.washingtonpost.com/news-sitemap-index.xml")
	bytes, _ := ioutil.ReadAll(resp.Body)
	xml.Unmarshal(bytes, &s)

	// looping to get into the locations
	for _, Location := range s.Locations {
		resp, _ := http.Get(Location)
		bytes, _ := ioutil.ReadAll(resp.Body)
		xml.Unmarshal(bytes, &n)
		// the output which is title locaiton and title must be a KVP like a dictionary
		for idx, _ := range n.Titles {
			news_map[n.Titles[idx]] = NewsMap{n.Keywords[idx], n.Locations[idx]}
		}

		p := NewsAggPage{Title: "News title 1", News: news_map} //  values in the page
		// template calling
		t, _ := template.ParseFiles("temapltefor17.html")
		// executing the page
		t.Execute(w, p) // writer and page
	}
}

func main() {
	http.HandleFunc("/", HomePageHandeller)
	// news aggregator page
	http.HandleFunc("/agg/", NewsAggHandeller) // this will use and execute the template
	http.ListenAndServe(":8000", nil)
}

// This whole thing is very slow, loadin gof the webiste takes 1ks of milisecs as the fetching of information happens serially.

/*

News title 1
News body 1


*/
