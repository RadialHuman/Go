package main

import (
	"fmt"
)

func main() {
	// var dic map[string]float32 // string will be the key and value will be float
	// map is just a reference type so no values in it
	// to add values "make" is being used
	// it can able be initiated with := since it is inside main
	Grades := make(map[string]float32)
	fmt.Println(Grades)
	Grades["one"] = 23
	Grades["two"] = 24
	Grades["three"] = 45
	fmt.Println(Grades)

	// style in Go is caps through out
	// can be changed using gofmt instead of go run in cmd
	TimsGrade := Grades["four"]
	fmt.Println(TimsGrade)
	TimsGrade = Grades["three"]
	fmt.Println(TimsGrade)

	delete(Grades, "one")
	fmt.Println(Grades)

	// looping through a map
	for k, v := range Grades {
		fmt.Println(k, ":", v)
	}

}

/*

map[]
map[one:23 two:24 three:45]
0
45
map[two:24 three:45]
two : 24
three : 45

*/
