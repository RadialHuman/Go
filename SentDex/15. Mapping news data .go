package main

import (
	"encoding/xml" // for parsing, builtin function, to "unmarshal"???
	"fmt"
	"io/ioutil"
	"net/http"
)

/*
If the site changes
var WashinPost = []byte(`
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<sitemap>
<loc>
http://www.washingtonpost.com/news-politics-sitemap.xml
</loc>
</sitemap>
<sitemap>
<loc>
http://www.washingtonpost.com/news-blogs-politics-sitemap.xml
</loc>
</sitemap>
<sitemap>
<loc>
http://www.washingtonpost.com/news-opinions-sitemap.xml
</loc>
</sitemap>`)
*/

// defining the structure of the sitemap, combining the previous structs and fucntion into one
type Sitemap struct {
	Locations []string `xml:"sitemap>loc"` // > loc inside the sitemap tag
}

// tyoe for news
type News struct {
	Titles    []string `xml:"url>news>title"`
	Keywords  []string `xml:"url>news>keywords"`
	Locations []string `xml:"url>loc"`
}

// since a map is of a certain type, we need to define struct which can take different types in
type NewsMap struct {
	Keyword  string
	Location string
}

func main() {
	// reading the articles inside sitemaps
	// example politics has title and keywords from the xml file

	var s Sitemap
	var n News
	// initiating the map (dictionary)
	news_map := make(map[string]NewsMap)

	resp, _ := http.Get("https://www.washingtonpost.com/news-sitemap-index.xml")
	bytes, _ := ioutil.ReadAll(resp.Body)
	xml.Unmarshal(bytes, &s)

	// looping to get into the locations
	for _, Location := range s.Locations {
		resp, _ := http.Get(Location)
		bytes, _ := ioutil.ReadAll(resp.Body)
		xml.Unmarshal(bytes, &n)
		// the output which is title locaiton and title must be a KVP like a dictionary
		for idx, _ := range n.Titles {
			news_map[n.Titles[idx]] = NewsMap{n.Keywords[idx], n.Locations[idx]}
		}
		for idx, data := range news_map {
			fmt.Println("\n", idx)
			fmt.Println("\n", data.Keyword)
			fmt.Println("\n", data.Location)
		}
	}

}

/*


                                        BBN-Mets-Mejia-Drug Suspension,New York City,New York,United States,North America,National League,New York Mets,NL East,Major League Baseball,Sports,Doping,Sports team and league operation,Sports business,Sports industry,Media and entertainment industry,Business,MLB baseball,mlb news,mlb 2018 news,mlb scores,mlb latest news,mlb highlights,mlb schedule 2018,2018 mlb schedule,Professional baseball,Baseball,Men's sports,Sports transactions

 https://www.washingtonpost.com/sports/nationals/mejia-conditionally-allowed-to-return-from-drug-ban-in-2019/2018/07/06/d0fa0eb0-815a-11e8-b3b5-b61896f90919_story.html

 Keys thinks about facing Serena next, but loses at Wimbledon


                                        TEN-Wimbledon-Keys Out,United Kingdom,Western Europe,Europe,Evgeniya Rodina,Madison Keys,Serena Williams,Sports,Women's tennis,Tennis,Women's sports,Wimbledon Championships

 https://www.washingtonpost.com/sports/tennis/keys-thinks-about-facing-serena-next-but-loses-at-wimbledon/2018/07/06/d5c67436-8148-11e8-b3b5-b61896f90919_story.html

 FAA: More testing to show jets can survive bird strike


                                        APFN-US-Jet Tests-Bird Strikes,Federal Aviation Administration,U.S. Department of Transportation,United States government,New York City,New York,United States,North America,Chesley B. Sullenberger III,Jeffrey Skiles,Business,General news,Birds,Animals,Aviation accidents and incidents,Transportation accidents,Accidents,Accidents and disasters,Transportation

 https://www.washingtonpost.com/national/faa-more-testing-to-show-jets-can-survive-bird-strike/2018/07/06/4d79eea6-8165-11e8-b3b5-b61896f90919_story.html

 1 killed in accident on Texas water tower


                                        US-Water Tower Accident,Texas,United States,North America,General news,Occupational accidents,Accidents,Accidents and disasters

 https://www.washingtonpost.com/national/1-killed-in-accident-on-texas-water-tower/2018/07/06/900002e8-8150-11e8-b3b5-b61896f90919_story.html

 South Sudan’s rival leaders sign agreement on security


                                        ML-South Sudan,South Sudan government,Riek Machar,Omar al-Bashir,Salva Kiir Mayardit,Sudan,North Africa,Africa,Middle East,Khartoum,South Sudan,General news,Armed forces,Military and defense,Government and politics,Army,Peace treaties,Treaties,International agreements,International relations,Peace process,Diplomacy

 https://www.washingtonpost.com/world/africa/sudan-says-south-sudan-rival-leaders-reach-deal-on-security/2018/07/06/3c492dd4-8126-11e8-b3b5-b61896f90919_story.html

 CFPB official drops fight for leadership of watchdog agency


                                        CFPB,Consumer Financial Protection Bureau,Mick Mulvaney,BCFP

 http://www.washingtonpost.com/news/business/wp/2018/07/06/cfpb-official-drops-fight-for-leadership-of-watchdog-agency/

 Do you support or oppose a recent Trump administration policy that separated immigrant children from their parents when the parent was accused of entering the U.S. illegally across the southern border? Do you feel that way strongly, or somewhat?


                                        immigration, polling, mueller, trump, children, separation

 https://www.washingtonpost.com/politics/polling/trump-administration-separated-immigrant/2018/07/06/f18cf3be-8122-11e8-b3b5-b61896f90919_page.html

 DE Lottery


                                        DE-LOT-Lottery Glance,Lotteries,General news

 https://www.washingtonpost.com/local/de-lottery/2018/07/06/f4799836-8148-11e8-b3b5-b61896f90919_story.html

 Herrmann’s HR helps lift Mariners over Angels 4-1


                                        BBA-Angels-Mariners,Washington,United States,North America,California,Tacoma,Seattle,Seattle Mariners,AL West,American League,Major League Baseball,Colorado Rockies,NL West,National League,Los Angeles Angels,German Marquez,Jabari Blash,Mike Zunino,Mitch Haniger,Matt Shoemaker,Andrelton Simmons,Nick Tropeano,Chris Young,Jim Johnson,David Fletcher,David Freitas,Jefry Marte,Ariel Miranda,Chris Herrmann,Justin Upton,Scott Servais,Mike Trout,Marco Gonzales,Jaime Barria,Shohei Ohtani,Albert Pujols,Guillermo Heredia,Marco Gonzales,Jean Segura,Dee Gordon,Cam Bedrosian,Miguel Almonte,Kole Calhoun,Jean,Colorado Rockies,Los Angeles Angels,Seattle Mariners,Seattle Mariners,Los Angeles Angels,Los Angeles Angels,Los Angeles Angels,Kansas City Royals,Los Angeles Angels,Los Angeles Angels,Seattle Mariners,Los Angeles Angels,Seattle Mariners,Seattle Mariners,Los Angeles Angels,Seattle Mariners,Los Angeles Angels,Seattle Mariners,Los Angeles Angels,Los Angeles Angels,Los Angeles Angels,Seattle Mariners,Seattle Mariners,Seattle Mariners,Los Angeles Angels,Kansas City Royals,Los Angeles Angels,Sports,MLB baseball,mlb news,mlb 2018 news,mlb scores,mlb latest news,mlb highlights,mlb schedule 2018,2018 mlb schedule,Professional baseball,Baseball,Men's sports,Athlete injuries,Athlete health

 https://www.washingtonpost.com/sports/nationals/herrmanns-hr-helps-lift-mariners-over-angels-4-1/2018/07/06/eaae67f4-80e0-11e8-a63f-7b5d2aba7ac5_story.html

*/
