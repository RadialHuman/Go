package main

import (
	"fmt"
)

// type in go can be inbuilt and can be user created
// like scala the parameters must have the type typed in with return type

func add(x float64, y float64) float64 {
	return x + y
}

func better_add(x, y float64) float64 {
	return x + y
}

func add_letters(x, y string) (string, string) {
	return x, y
}

// all the variables must be used or else error will be thrown
// random constant definition
const s string = "something"

func main() {
	// define a veriable
	var num1 float64 = 4.5
	var num2 float64 = 5.4
	fmt.Println("The result of addition is", add(num1, num2))

	// concise way of declaring
	var num3, num4 float64 = 5.5, 6.4
	fmt.Println("The result of better addition is", better_add(num3, num4))

	// complier will understand the type and assign it, but its default is float64 so , the function cant be for float32
	num5, num6 := 6.5, 7.4
	fmt.Println("The result of best addition is", better_add(num5, num6))

	t := "new"
	fmt.Println(add_letters(s, t))

	// type transfer
	var a int = 5
	var b float64 = float64(a)
	x := a
	fmt.Printf("%T\n%T\n%T\n", a, b, x)
}

/*

The result of addition is 9.9
The result of better addition is 11.9
The result of best addition is 13.9
something new
int
float64
int

*/
