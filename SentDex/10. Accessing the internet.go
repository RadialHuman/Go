package main

// pull data from the internet

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	// get info from the internet
	// data comes in bytes, which is to be converted to string to parse
	// making a request returns a response and err if something goes down
	// error will not be used so placeholder
	resp, _ := http.Get("https://arxiv.org/abs/1806.11544v1") // ask for data from site
	bytes, _ := ioutil.ReadAll(resp.Body)                     // read as bytes
	fmt.Println(string(bytes))
	resp.Body.Close() // free up the resources

}

/*

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<title>[1806.11544v1] Nonparametric learning from Bayesian models with randomized objective
  functions</title>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" media="screen" href="/css/arXiv.css?v=20170424" />
<meta name="citation_title" content="Nonparametric learning from Bayesian models with randomized objective functions" />
<meta name="citation_author" content="Lyddon, S. P." />
<meta name="citation_author" content="Walker, S. G." />
<meta name="citation_author" content="Holmes, C. C." />
<meta name="citation_date" content="2018/06/29" />
<meta name="citation_online_date" content="2018/06/29" />
<meta name="citation_pdf_url" content="http://arxiv.org/pdf/1806.11544" />
<meta name="citation_arxiv_id" content="1806.11544" />
<script src="/js/mathjaxToggle.min.js" type="text/javascript"></script>

<!-- Piwik -->
<script type="text/javascript">
var _paq = _paq || [];
_paq.push(["setDomains", ["*.arxiv.org"]]);
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function()
{ var u="//webanalytics.library.cornell.edu/"; _paq.push(['setTrackerUrl', u+'piwik.php']); _paq.push(['setSiteId', 538]); var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s); }
)();
</script>
<!-- End Piwik Code -->


</head>
<body class="with-cu-identity">

<noscript><img src="//webanalytics.library.cornell.edu/piwik.php?idsite=538&rec=1" style="border:0;" alt="" /></noscript>

<div id="cu-identity">
<div id="cu-logo">
<a id="insignia-link" href="http://www.cornell.edu/"><img src="/icons/cu/cul_signature_unstyled.gif" alt="Cornell University" width="283" height="76" border="0" /></a>
<div id="unit-signature-links">
<a id="cornell-link" href="http://www.cornell.edu/">Cornell University</a>
<a id="unit-link" href="http://www.library.cornell.edu/">Library</a>
</div>
</div>
<div id="support-ack">
<a href="https://confluence.cornell.edu/x/ALlRF">We gratefully acknowledge support from<br />the Simons Foundation<br /> and member institutions</a>
</div>
</div>
<div id="header">
<h1><a href="/">arXiv.org</a> &gt; <a href="/list/stat/recent">stat</a> &gt; arXiv:1806.11544v1</h1>
<div id="search">
<form id="search-arxiv" method="GET" action="/search">

<div class="wrapper-search-arxiv">
<input class="keyword-field" type="text" name="query" placeholder="Search or Article ID"/>

<div class="filter-field">
<select name="searchtype">
<option value="all">All fields</option>
<option value="title">Title</option>
<option value="author">Author(s)</option>
<option value="abstract">Abstract</option>
<option value="comments">Comments</option>
<option value="journal_ref">Journal reference</option>
<option value="acm_class">ACM classification</option>
<option value="msc_class">MSC classification</option>
<option value="report_num">Report number</option>
<option value="paper_id">arXiv identifier</option>
<option value="doi">DOI</option>
<option value="orcid">ORCID</option>
<option value="author_id">arXiv author ID</option>
<option value="help">Help pages</option>
<option value="full_text">Full text</option></select>
</div>
<input class="btn-search-arxiv" value="" type="submit">
<div class="links">(<a href="/help">Help</a> | <a href="/search/advanced">Advanced search</a>)</div>
</div>
<input type="hidden" name="source" value="header">
</form>
</div>
</div>
<div id="content">

<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:dc="http://purl.org/dc/elements/1.1/"
         xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
    <rdf:Description
        rdf:about="http://arxiv.org/abs/1806.11544"
        dc:identifier="http://arxiv.org/abs/1806.11544"
        dc:title="Nonparametric learning from Bayesian models with randomized objective
  functions"
        trackback:ping="http://arxiv.org/trackback/1806.11544" />
    </rdf:RDF>
-->

<div id="abs">
<div class="extra-services">

<div class="full-text">
<span class="descriptor">Full-text links:</span>
<h2>Download:</h2>
<ul>
<li><a href="/pdf/1806.11544v1" accesskey="f">PDF</a></li>
<li><a href="/format/1806.11544v1">Other formats</a></li>
</ul>
<div class="abs-license">(<a href="http://arxiv.org/licenses/nonexclusive-distrib/1.0/" title="Rights to this article">license</a>)</div>
</div><!--end full-text-->

<div class="browse">
<h3>Current browse context:</h3>
<div class="current">stat.ML</div>
<div class="prevnext"><span class="arrow"><a href="http://arxiv.org/prevnext?site=arxiv.org&amp;id=1806.11544&amp;context=stat.ML&amp;function=prev" accesskey="p" title="previous in stat.ML (accesskey p)">&lt;&nbsp;prev</a></span>&nbsp;|&nbsp;<span class="arrow"><a href="http://arxiv.org/prevnext?site=arxiv.org&amp;id=1806.11544&amp;context=stat.ML&amp;function=next" accesskey="n" title="next in stat.ML (accesskey n)">next&nbsp;&gt;</a></span>
<br /></div>
<div class="list"><a href="/list/stat.ML/new">new</a>&nbsp;| <a href="/list/stat.ML/recent">recent</a>&nbsp;| <a href="/list/stat.ML/1806">1806</a></div><h3>Change to browse by:</h3><div class="switch">
<a href="/abs/1806.11544?context=cs">cs</a><br />
<span class="subclass"><a href="/abs/1806.11544?context=cs.LG">cs.LG</a></span><br />
<a href="/abs/1806.11544?context=stat">stat</a><br />
<span class="subclass"><a href="/abs/1806.11544?context=stat.ME">stat.ME</a></span>
</div>

</div>
<div class="extra-ref-cite">
<h3>References &amp; Citations</h3><ul><li><a href="http://adsabs.harvard.edu/cgi-bin/bib_query?arXiv:1806.11544">NASA ADS</a></li>
</ul>

</div>
<div class="bookmarks">
<div class="what-is-this">
<h3>Bookmark</h3> (<a href="/help/social_bookmarking">what is this?</a>)
</div>
<a href="/ct?url=http%3A%2F%2Fwww.citeulike.org%2Fposturl%3Furl%3Dhttp%3A%2F%2Farxiv.org%2Fabs%2F1806.11544%26title%3DNonparametric%2520learning%2520from%2520Bayesian%2520models%2520with%2520randomized%2520objective%250A%2520%2520functions%26authors%3D&amp;v=a76936f0" title="Bookmark on CiteULike"><img src="//static.arxiv.org/icons/social/citeulike.png" alt="CiteULike logo" /></a>
<a href="/ct?url=http%3A%2F%2Fwww.bibsonomy.org%2FBibtexHandler%3FrequTask%3Dupload%26url%3Dhttp%3A%2F%2Farxiv.org%2Fabs%2F1806.11544%26description%3DNonparametric%2520learning%2520from%2520Bayesian%2520models%2520with%2520randomized%2520objective%250A%2520%2520functions&amp;v=ab242308" title="Bookmark on BibSonomy"><img src="//static.arxiv.org/icons/social/bibsonomy.png" alt="BibSonomy logo" /></a>
<a href="/ct?url=https%3A%2F%2Fwww.mendeley.com%2Fimport%2F%3Furl%3Dhttp%3A%2F%2Farxiv.org%2Fabs%2F1806.11544&amp;v=1f15b86c" title="Bookmark on Mendeley"><img src="//static.arxiv.org/icons/social/mendeley.png" alt="Mendeley logo" /></a>
<a href="/ct?url=https%3A%2F%2Fdel.icio.us%2Fpost%3Furl%3Dhttp%3A%2F%2Farxiv.org%2Fabs%2F1806.11544%26description%3DNonparametric%2520learning%2520from%2520Bayesian%2520models%2520with%2520randomized%2520objective%250A%2520%2520functions&amp;v=93d0538a" title="Bookmark on del.icio.us"><img src="//static.arxiv.org/icons/social/delicious.png" alt="del.icio.us logo" /></a>
<a href="/ct?url=https%3A%2F%2Fdigg.com%2Fsubmit%3Furl%3Dhttp%3A%2F%2Farxiv.org%2Fabs%2F1806.11544%26title%3DNonparametric%2520learning%2520from%2520Bayesian%2520models%2520with%2520randomized%2520objective%250A%2520%2520functions&amp;v=23cea0c7" title="Bookmark on Digg"><img src="//static.arxiv.org/icons/social/digg.png" alt="Digg logo" /></a>
<a href="/ct?url=https%3A%2F%2Freddit.com%2Fsubmit%3Furl%3Dhttp%3A%2F%2Farxiv.org%2Fabs%2F1806.11544%26title%3DNonparametric%2520learning%2520from%2520Bayesian%2520models%2520with%2520randomized%2520objective%250A%2520%2520functions&amp;v=36d8ef12" title="Bookmark on Reddit"><img src="//static.arxiv.org/icons/social/reddit.png" alt="Reddit logo" /></a>
<a href="/ct?url=http%3A%2F%2Fsciencewise.info%2Fbookmarks%2Fadd%3Furl%3Dhttp%3A%2F%2Farxiv.org%2Fabs%2F1806.11544&amp;v=a59c5176" title="Bookmark on ScienceWISE"><img src="//static.arxiv.org/icons/social/sciencewise.png" alt="ScienceWISE logo" /></a>

</div>
</div><!--end extra-services-->

<div class="leftcolumn">
<div class="subheader">
<h1>Statistics > Machine Learning</h1>
</div>
<h1 class="title mathjax"><span class="descriptor">Title:</span>
Nonparametric learning from Bayesian models with randomized objective  functions</h1>
<div class="authors"><span class="descriptor">Authors:</span>
<a href="/search?searchtype=author&query=Lyddon%2C+S+P">S. P. Lyddon</a>,
<a href="/search?searchtype=author&query=Walker%2C+S+G">S. G. Walker</a>,
<a href="/search?searchtype=author&query=Holmes%2C+C+C">C. C. Holmes</a></div>
<div class="dateline">(Submitted on 29 Jun 2018)</div>
<blockquote class="abstract mathjax">
<span class="descriptor">Abstract:</span> Bayesian learning is built on an assumption that the model space contains a
true reflection of the data generating mechanism. This assumption is
problematic, particularly in complex data environments. Here we present a
Bayesian nonparametric approach to learning that makes use of statistical
models, but does not assume that the model is true. Our approach has provably
better properties than using a parametric model and admits a trivially
parallelizable Monte Carlo sampling scheme that affords massive scalability on
modern computer architectures. The model-based aspect of learning is
particularly attractive for regularizing nonparametric inference when the
sample size is small, and also for correcting approximate approaches such as
variational Bayes (VB). We demonstrate the approach on a number of examples
including VB classifiers and Bayesian random forests.
</blockquote>
<!--CONTEXT-->
<div class="metatable">
<table summary="Additional metadata">
<tr>
<td class="tablecell label">Subjects:
</td>
<td class="tablecell subjects"><span class="primary-subject">Machine Learning (stat.ML)</span>; Machine Learning (cs.LG); Methodology (stat.ME)</td>
</tr>
<tr>
<td class="tablecell label">
Cite&nbsp;as:
</td>
<td class="tablecell arxivid"><a href="/abs/1806.11544">arXiv:1806.11544</a> [stat.ML]</td>
</tr>
<tr>
<td class="tablecell label">&nbsp;</td>
<td class="tablecell arxividv">(or <span class="arxivid"><a href="/abs/1806.11544v1">arXiv:1806.11544v1</a> [stat.ML]</span> for this version)</td>
</tr>
</table>
</div>
<div class="submission-history">
<h2>Submission history</h2>
From: Simon Lyddon [<a href="https://arxiv.org/show-email/1a3569d0/1806.11544">view email</a>]
<br />
<b>[v1]</b> Fri, 29 Jun 2018 17:18:28 GMT  (276kb,D)<br />
</div>
<div class="endorsers"><a href="http://arxiv.org/auth/show-endorsers/1806.11544">Which authors of this paper are endorsers?</a> | <a id="mathjax_toggle" href="javascript:setMathjaxCookie()">Disable MathJax</a> (<a href="/help/mathjax/">What is MathJax?</a>)</div><script type="text/javascript" language="javascript">mathjaxToggle();</script>
</div><!--end leftcolumn-->
</div><!--end abs-->
</div>
<div id="footer">
<div class="footer-text">
<p>Link back to: <a href="http://arxiv.org/">arXiv</a>, <a href="/form">form interface</a>, <a href="/help/contact">contact</a>.</p>
</div>
<div class="social"><a href="https://twitter.com/arxiv"><img src="//static.arxiv.org/icons/twitter_logo_blue.png" alt="Twitter" title="Follow arXiv on Twitter"></a></div>
</div>
</body>
</html>


*/
