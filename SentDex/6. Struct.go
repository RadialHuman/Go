package main

import (
	"fmt"
)

// there are no classes in go, instead there are structs
// go has methods which are associated to structs

// example : A car that can be controleld by controller
type car struct {
	gas_pedal      uint16
	break_pedal    uint16
	steering_wheel int16
	top_speed_kmph float64
}

func main() {
	// var a_car car, is an option, but
	a_car := car{gas_pedal: 123,
		break_pedal:    0,
		steering_wheel: 12345,
		top_speed_kmph: 300.0}

	// a_car := car{123,0,12345,300.0} can also be done
	fmt.Println(a_car.gas_pedal)
}

/*

123

*/
