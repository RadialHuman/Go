package main

import ("sync"
		"net/http"
		)

// temp1 represents single template

type templateHandler struct{
	once sync.Once
	filename string
	temp1 *template.Template
}

// function to handle http request

func (t *templateHandler) ServerHTTP(w http.ResponseWriter, r*http.Request) {
	t.once.Do(func() {
		t.temp1= template.Must(template.ParseFiles(filepath.join("templates",t.filename)))
	})
	t.temp1.Execute(w, nil)
}


func main(){
	http.Handel("/",&templateHandler{filename: "lets_chat.html"})
	if err := http.ListenAndServe(":8080", nil); err != nil {
	log.Fatal("ListenAndServe:", err)
	}
}