package main

import (
	"fmt"
	"net/http"
)

func index_handler(w http.ResponseWriter, r *http.Request) { // widely used "w" response writter is is custom
	fmt.Fprintf(w, "Something")
}

func about_handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Something more")
}

func main() { //handeler takes the url entered and matches with its corresponding function
	http.HandleFunc("/", index_handler) //means the home page and its fucntion
	http.HandleFunc("/about/", about_handler)
	http.ListenAndServe(":8001", nil) // port number and server stuff
}

// check the output in http://127.0.0.1:8001/
// part 5 ended
