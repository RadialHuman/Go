/*
Idea
> Read all links from excel file
> Get body of the news articles
> Save it to excel (pending)
*/
package main

import (
	"bufio"
	// "bytes"
	"encoding/csv"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/advancedlogic/GoOse"
	// "github.com/tealeg/xlsx"
	"io"
	"log"
	"os"
	"strings"
)

// type body struct {
// 	name string
// }

func main() {
	// var content []string
	content := make([][]string, 0, 6000)
	var x []string
	// Reading CSV
	f, _ := os.Open("//lntdatacenter/COEA_TEAM/LTC_AnalyticsData/PROJ0003_Vendor_Profiling/GOOGLE/file_4_go.csv")
	// reading the file row wise
	r := csv.NewReader(bufio.NewReader(f))
	for {
		record, err := r.Read()
		// Stop at EOF.
		if err == io.EOF {
			break
		}
		for value := range record {
			if strings.Contains(record[value], "google") {
				// fmt.Printf("  %T\n", record[value])
				pseudo_url := record[value]
				// gettting the real link
				doc, err := goquery.NewDocument(pseudo_url)
				if err != nil {
					log.Fatal(err)
				}
				// finding the real link
				doc.Find("a[href]").Each(func(index int, item *goquery.Selection) {
					href, _ := item.Attr("href")
					if len(href) == len(item.Text()) {
						// fmt.Println(href)
						g := goose.New()
						article, err := g.ExtractFromURL(href)
						if err != nil {
							return
						}
						fmt.Println(len(article.CleanedText))
						if len(article.CleanedText) > 0 {
							x = append(x, article.CleanedText)
						} else {
							x = append(x, "")
						}
						content = append(content, x)
						x = nil
						fmt.Printf("%v\n", len(content))
					}

				})

			}
		}

	}

	// write content into csv file
	csvWriter(content)
	// fmt.Println(content)
}

func csvWriter(yourSliceGoesHere [][]string) {
	f, err := os.Create("//lntdatacenter/COEA_TEAM/LTC_AnalyticsData/PROJ0003_Vendor_Profiling/GOOGLE/file_from_go.csv")
	// f, err := os.OpenFile("//lntdatacenter/COEA_TEAM/LTC_AnalyticsData/PROJ0003_Vendor_Profiling/GOOGLE/file_from_go.csv", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	// f, err := os.OpenFile("//lntdatacenter/COEA_TEAM/LTC_AnalyticsData/PROJ0003_Vendor_Profiling/GOOGLE/file_from_go.csv", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}
	w := csv.NewWriter(f)
	for i := 0; i < len(yourSliceGoesHere); i++ {
		w.Write(yourSliceGoesHere[i])
	}
	w.Flush()
}

// to avoid runtime error of invalid memory address or nil pointer dereference
// func panicker() {
// 	fmt.Println("about to panic")
// 	defer func() {
// 		if err := recover(); err != nil {
// 			log.Println("Error:", err)
// 			panic(err)
// 		}
// 	}()
// 	panic("something bad happened")
// 	fmt.Println("Done panicking")
// }
