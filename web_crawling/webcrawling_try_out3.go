package main

import (
	"fmt"
	"github.com/advancedlogic/GoOse"
	// "github.com/tealeg/xlsx"
)

func main() {
	// Reading the excel file
	// xldf := "//lntdatacenter/COEA_TEAM/LTC_AnalyticsData/PROJ0003_Vendor_Profiling/Vendor Profiling_3006/vendor Data Availability_output_44.xlsx"
	// xlFile, err := xlsx.OpenFile(xldf)
	// if err == nil {
	// 	fmt.Println("File read")
	// }
	// // fmt.Println(xlFile[0])

	// for _, sheet := range xlFile.Sheets {
	// 	for _, row := range sheet.Rows {
	// 		// for _, cell := range row.Cells {
	// 		// text := cell.String()

	// 		for i := 1; i <= len(row.Cells); i += 3 {
	// 			// fmt.Println(row.Cells[i].String())
	// 			g.ExtractFromURL("https://news.google.com/")
	// 		}
	// 		// }
	// 	}
	// }

	// Reading in the content from links
	g := goose.New()
	article, _ := g.ExtractFromURL("https://schier.co/blog/2015/04/26/a-simple-web-scraper-in-go.html")
	fmt.Println("**** title", article.Title)
	fmt.Println("**** description", article.MetaDescription)
	fmt.Println("**** keywords", article.MetaKeywords)
	fmt.Println("**** content", article.CleanedText)

}
