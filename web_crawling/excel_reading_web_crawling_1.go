/*
Idea
> Read all vendor names from excel file
> Get all the links, headline, source, time from google related to the vendor
> Save it to excel (pending)
*/

package main

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/PuerkitoBio/goquery"
	"io/ioutil"
	"net/http"
	"strings"
)

func main() {

	var row [][]string
	var url string
	var google_url, title, source, time []string

	// Reading the excel into variable xlsx
	xlsx, err := excelize.OpenFile("//lntdatacenter/COEA_TEAM/LTC_AnalyticsData/PROJ0003_Vendor_Profiling/Vendor Profiling_3006/vendor Data Availability_output_44.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}

	// reading the file row wise
	row = xlsx.GetRows("Final vendor list")
	// Get value from cell by given worksheet name and axis.
	for i := 1; i < 3; i++ {
		url = "https://news.google.com/search?q=" + "\"" + row[i][1] + "\""
		url = strings.Replace(url, " ", "%20", -1)
		fmt.Printf("HTML code of %s ...\n", url)
		resp, err := http.Get(url)
		// handle the error if there is one
		if err != nil {
			panic(err)
		}
		// do this now so it won't be forgotten
		defer resp.Body.Close()
		// reads html as a slice of bytes
		html, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		// show the HTML
		// fmt.Printf("%v\n", html)

		// extracting the required text
		p := strings.NewReader(string(html))
		doc, _ := goquery.NewDocumentFromReader(p)

		// Getting headline and google link
		doc.Find("a[href]").Each(func(index int, item *goquery.Selection) {
			href, _ := item.Attr("href")
			if strings.Contains(href, "./articles") && strings.Contains(item.Text(), " ") {
				// fmt.Printf("link: %s - anchor text: %s\n", href, item.Text())
				google_url = append(google_url, href)
				title = append(title, item.Text())

			}
		})

		// Getting the time of the news
		doc.Find("time").Each(func(index int, item *goquery.Selection) {
			time = append(time, item.Text())
		})

		// Getting the source
		doc.Find(".KbnJ8").Each(func(index int, item *goquery.Selection) {
			source = append(source, item.Text())
		})
	}
	fmt.Println(len(source), len(time), len(title), len(google_url))
}
