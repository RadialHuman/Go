/*
Idea
> Read all links from excel file
> Get body of the news articles
> Save it to excel (pending)
*/
package main

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/advancedlogic/GoOse"
	"strings"
)

func main() {

	var row [][]string
	var url string

	// Reading the excel into variable xlsx
	xlsx, err := excelize.OpenFile("//lntdatacenter/COEA_TEAM/LTC_AnalyticsData/PROJ0003_Vendor_Profiling/GOOGLE/file_4_go.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
	// reading the file row wise
	row = xlsx.GetRows("Sheet1")

	// Get value from cell by given worksheet name and axis.
	for i := 1; i < len(row[i]); i++ {
		fmt.Println(row[i])
		// url = "https://news.google.com/search?q=" + "\"" + row[i][1] + "\""
		// url = strings.Replace(url, " ", "%20", -1)
		url = row[i][3]
		g := goose.New()
		article, _ := g.ExtractFromURL(url)
		fmt.Println("**** title", article.Title)
		fmt.Println("**** description", article.MetaDescription)
		// fmt.Println("**** keywords", article.MetaKeywords)
		fmt.Println("**** content", article.CleanedText)
		// fmt.Println("**** ", article.FinalURL)

	}

}
