package main

import "fmt"

func main() {
	colors := map[string]string{
		"key1": "value1",
		"key2": "value2",
		"key3": "value3",
		"key4": "value4",
	} // decalration of map with key first and then values
	fmt.Println(colors)

	// another way of declaring
	var colors2 map[string]string
	fmt.Println(colors2)

	// another way of declaring
	colors3 := make(map[string]string)
	colors3["key1"] = "value1" // adding to maps
	fmt.Println(colors3)
	delete(colors3, "key1")
	fmt.Println(colors3)
	printAMap(colors)
}

// iterating over a map

func printAMap(s map[string]string) {
	for k, v := range s {
		fmt.Println(k, v)
	}
}
