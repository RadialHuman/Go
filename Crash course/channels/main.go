package main

import (
	"fmt"
	"net/http"
)

// this program is a status checker for popular websites using http request
// with channels and goroutines

func main() {
	links := []string{
		"https://google.com",
		"https://facebook.com",
		"https://amazon.com",
		"https://golang.org",
		"https://stackoverflow.com",
	}

	c := make(chan string) // creating channel

	for _, link := range links {
		go checkLink(link, c) // just this addition is enough to create new go routines, apart from  the default one
		// the main go routine is default and the rest are child and are less respected, thus with this alone there is no output
		// main routine runs and checks it there is anythin else, disregarding the child routines

		// channels are used to communicate between main and childrens
		// they are typed so only the same type data can be passed around
		fmt.Println(<-c)
	}

}

func checkLink(l string, ch chan string) {
	_, err := http.Get(l)
	if err != nil {
		fmt.Println("Something is wrong with:", l)
		ch <- "Down"
		return
	}
	fmt.Println(l, "is up!")
	ch <- "it works"
}
