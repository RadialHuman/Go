package main

import "testing"

// testing file is to test the other go files, written in go.

// to check basic things like if the number of cards created is right,
// the first card is ace of spades
// test fucntions depends on what has to be checked and is logical as per the requirements

func TestNewDeck(t *testing.T) { // the argument is default, its a test handler
	d := newDeck()
	if len(d) != 52 {
		t.Errorf("Expected length of 52 but received %v", len(d))
	}

	if d[0] != "Ace of Spade" {
		t.Errorf("Expected Ace of Spade as the first card but received %v", d[0])
	}

	if d[len(d)-1] != "king of Club" {
		t.Errorf("Expected Ace of Spade as the first card but received %v", d[len(d)-1])
	}
}
