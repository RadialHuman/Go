package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

/*
new type called deck which is a slice of strings
*/

type deck []string // deck borrows the behaviour of the string slice now, this can now be used as a custom type in main

func newDeck() deck { // no receiver as there is no input and this si the one that creates cards
	cards := deck{}
	// to create deck
	// not a card so just a slice and not deck
	cardSuits := []string{"Spade", "Diamond", "Hearts", "Club"}
	cardValue := []string{"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king"}
	for _, cs := range cardSuits {
		for _, cv := range cardValue {
			cards = append(cards, cv+" of "+cs)
		}
	}
	return cards
}

func (d deck) printFunction() { // reciever function, which makes any variable with type deck access to print, d is the parameter passed to the function
	for _, card := range d { // reciever variable d is generally a 1/2 word abreviation of the type it belongs to
		fmt.Println(card)
	}
}

// dealing a deck of cards
func deal(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:]
}

// helper function to turn a deck to a string
func (d deck) toString() string {
	return strings.Join([]string(d), ",") // changing from deck to slice of string type
}

// write a byteslice into file
func (d deck) saveToFile(filename string) error {
	return ioutil.WriteFile(filename, []byte(d.toString()), 0666) // 0666 gives permission for anyone to read and write
}

// take file from hard disk and change it to a deck
func newDeckFromFile(filename string) deck {
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		// error handlling: log the error and stop the program as somethign has gone wrong
		fmt.Println("Error: ", err)
		os.Exit(1) // 0 means nothign went wrong
	}
	ss := strings.Split(string(bs), ",") // converting byte slcie to string and splitting it to slice of string
	// slice of string to deck
	return deck(ss)
}

// to shuffle the cards,loop through the cards and randomly generate number
func (d deck) shuffle() { // this shuffle function gives the same value again and again, and thats an issue
	for i := range d {
		newPosition := rand.Intn(len(d) - 1)        // psuedo random generator which works on seed
		d[i], d[newPosition] = d[newPosition], d[i] // swaping line
	}
}

func (d deck) shuffle2() { // this shuffle function gives real random number
	// to create int64 which is different everytime
	t := time.Now().UnixNano()
	// random number generator
	source := rand.NewSource(t)
	r := rand.New(source)
	for i := range d {
		newPosition := r.Intn(len(d) - 1)
		d[i], d[newPosition] = d[newPosition], d[i]
	}
}
