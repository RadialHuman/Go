package main

import "fmt"

func main() {
	// aim is to replicate a deck of cards and shuffle it, deal a hand and save & retrive it from local

	// var card string = "Ace of spades"
	// another way fo doing the same
	card := "Some string here, so that it can be reassigned"
	// if the variable's value has to be changed
	card = "Ace of spades"
	// using function
	card = newCard()
	fmt.Println(card)

	// making a list of cards
	// slice of string
	// cards := []string{"first card", newCard(), "second card"}
	cards := deck{"first card", newCard(), "second card"}
	fmt.Println(cards)
	// adding a new element to the slice, does not append to the original, instead creates new one and assigns it to the variable
	// cards = append(cards, newCard())
	// after the deck.go we can use
	deckCards := newDeck()
	// iterating over slice and printing
	// for i, c := range cards { // for loop reinitializes as the previous one is dropped
	// 	fmt.Println(i, c)
	// }
	// using the deck.go's function
	fmt.Println("\nThe deck is:")
	deckCards.printFunction()
	fmt.Println("\nFirst hand is:")
	hand1, hand2 := deal(deckCards, 3)
	hand1.printFunction()
	fmt.Println("\nSecond hand is:")
	hand2.printFunction()
	fmt.Println("\nString of the deck:")
	fmt.Println(deckCards.toString())
	deckCards.saveToFile("deck_of_cards.txt")
	fmt.Println("\nThe deck from file is:")
	c := newDeckFromFile("deck_of_cards.txt")
	c.printFunction()

	fmt.Println("\nThe shuffled deck is :")
	// c.shuffle() // psuedo random
	c.shuffle2()
	c.printFunction()
}

func newCard() string { // returning datatype from the function
	return "2 Ace of spades"
}
