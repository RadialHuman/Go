package main

import "fmt"

// interfaces are to avoid writing functions that need modification for each type it gets as arg or receiver
// it is a generic way for all types
// to also avoid overloading of functions

// sample program is to create 2 chatbots eng and esp
// two types, with 2 functions for greeting and printing it

type englishBot struct{} // no fields, juts to create functions

type spanishBot struct{} // no fields, juts to create functions

func main() {
	// strucnts declared
	eb := englishBot{}
	// sb := spanishBot{}
	printGreeting(eb)
	// printGreeting(sb)

}

// creating two functions with their own logic but same type
func (eb englishBot) getGreeting() string { // the values in the reciever can be removed as they are not being used in the function
	// this can work only with englishBot struct
	return ("Hello")
}

func (sb spanishBot) getGreeting() string { // the values in the reciever can be removed as they are not being used in the function
	// this can work only with englishBot struct
	return ("Hola")
}

// creating functions to print out the greeting, with similar logic
func printGreeting(eb englishBot) {
	fmt.Println(eb.getGreeting())
}

/*
func printGreeting(sb englishBot) { // overloading , making same named functions are prohibitted
	fmt.Println(sb.getGreeting())
}

*/
