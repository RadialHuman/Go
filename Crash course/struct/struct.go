package main

// structs are some what like dictionaries
import (
	"fmt"
)

type person struct {
	firstName string
	lastName  string
}

type address struct {
	zip   int
	email string
}

type human struct {
	firstName string
	lastName  string
	contact   address // this can be just address which would create a variable which will be address type and address name
}

func main() {
	// syntax 1
	alex := person{"Alex", "Jones"}
	fmt.Println(alex.firstName)

	// syntax 2
	alex = person{firstName: "Alex", lastName: "Jones"}
	fmt.Println(alex.firstName)

	// another way to show all the components, string formatting
	fmt.Printf("%v\n", alex)

	// zero value is as defined by the basic type
	var none person
	fmt.Printf("%v\n", none)
	none.firstName = "No"
	none.lastName = "thing"
	fmt.Printf("%v\n", none)

	// struct inside struct variable
	newName := human{
		firstName: "name",
		lastName:  "name2",
		contact: address{
			email: "email@email.com",
			zip:   23456,
		},
	}
	// fmt.Println(newName)
	// using function to print details
	newName.details()
	fmt.Println(newName.contact.email)

	newNamePointer := &newName
	newNamePointer.updateName("name3") // this will not be updated as pointers are required, as the update was done to the copy of the variable
	newName.details()

	// or as long as the function is the way it is, even this one is fine
	newName.updateName("Name4")
	newName.details()
}

// function to get into struct vairable and get details of a person

func (h human) details() {
	fmt.Println(h)
}

// function to update the name of the person in struct
func (pointerToHuman *human) updateName(newFirstName string) {
	(*pointerToHuman).firstName = newFirstName
}
