package main

import (
	"fmt"
	"math"
	"math/rand" // sub package in math
)

func ex() { // works only when called by the main
	fmt.Println("This will show the root of a number:")
}

func ex2() {
	fmt.Println("This will show any random number: \n", rand.Intn(12))
}

func main() { // only the main function runs defaultly
	ex()
	fmt.Println("Square root of 25 is", math.Sqrt(25))
	ex2()
} // capitalized letters are exported, if not its internal

// to know more about a package or command
// > godoc fmt Println
