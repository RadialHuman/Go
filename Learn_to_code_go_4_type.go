// https://www.youtube.com/playlist?list=PLSak_q1UXfPp971Hgv7wHCU2gDOb13gBQ

package main

// static programming language, type is imp
// it compiled so its fast and ease of programming

import (
	"fmt"
)

var y int = 23

var z int // declaration

func main() {

	x := 34 // in this case compiler knows wat to do
	z = 21  // assignment
	fmt.Println(x, y, z)

}
