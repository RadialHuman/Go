// https://www.youtube.com/playlist?list=PLSak_q1UXfPp971Hgv7wHCU2gDOb13gBQ

package main

import (
	"fmt"
)

// if a variable is declared but there is no initialization, the compiler assigns zero value automatically

var z int
var y string
var x bool

func main() {
	fmt.Println(z) // prints the zero type of int which is 0
	fmt.Println(y) // prints the zero type of string which is  ""
	fmt.Println(x) // prints the zero type of bool which is false
}
