// https://www.youtube.com/playlist?list=PLSak_q1UXfPp971Hgv7wHCU2gDOb13gBQ

package main

import (
	"fmt"
)

// boolean

// zero value of bool is false

var x bool

func main() {

	fmt.Println(x)
	x = true
	fmt.Println(x)

	y := true
	fmt.Println(y)

	a := 23
	b := 32
	fmt.Println(a == b)
	z := fmt.Sprint(a == b)
	fmt.Println(z)

}

/*

false
true
true
false
false


*/
