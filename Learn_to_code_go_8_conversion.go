// https://www.youtube.com/playlist?list=PLSak_q1UXfPp971Hgv7wHCU2gDOb13gBQ

package main

import (
	"fmt"
)

// go is all about types, static programming language

var a int

type integer int // creating own type

var b integer // using own type

func main() {

	a = 42
	fmt.Println(a)
	fmt.Printf("%T\n", a)

	b = 24
	fmt.Println(b)
	fmt.Printf("%T\n", b) // shows own type as under the main function

	// since go is static, a and b , even though intergers are not of the same type and cant be interchanged
	// a = b not possible

	// since interger has int underlying, b can be changed to type fo a by
	a = int(b) // others call it casting, here it is conversion
	fmt.Println(a, b)
	fmt.Printf("%T\n%T\n", a, b)
	fmt.Printf("%T\n%T\n", a, int(b))

}

/*
42
int
24
main.integer
24 24
int
main.integer
int
int


*/
