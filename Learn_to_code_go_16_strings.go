// https://www.youtube.com/playlist?list=PLSak_q1UXfPp971Hgv7wHCU2gDOb13gBQ

package main

import (
	"fmt"
)

func main() {

	s := `Multiple line
	 string literal`
	fmt.Println(s)
	fmt.Printf("%T\n", s)

	bs := []byte(s) // converting a string to slice of byte - ascii coding result as per utf-8
	fmt.Printf("%T\n", bs)
	fmt.Println(bs)

	for i := 0; i < len(s); i++ {
		fmt.Printf("%#U\n", s[i]) // to show utf-8 insides
	}

	for i, v := range s {
		fmt.Println(i, v) // shows the byte value indexed
	}

	for i, v := range s {
		fmt.Printf(" %d  %#x \n", i, v) // shows the hexadecimal value indexed
	}
}

/*

Multiple line
	 string literal
string
[]uint8
[77 117 108 116 105 112 108 101 32 108 105 110 101 10 9 32 115 116 114 105 110 103 32 108 105 116 101 114 97 108]
U+004D 'M'
U+0075 'u'
U+006C 'l'
U+0074 't'
U+0069 'i'
U+0070 'p'
U+006C 'l'
U+0065 'e'
U+0020 ' '
U+006C 'l'
U+0069 'i'
U+006E 'n'
U+0065 'e'
U+000A
U+0009
U+0020 ' '
U+0073 's'
U+0074 't'
U+0072 'r'
U+0069 'i'
U+006E 'n'
U+0067 'g'
U+0020 ' '
U+006C 'l'
U+0069 'i'
U+0074 't'
U+0065 'e'
U+0072 'r'
U+0061 'a'
U+006C 'l'
0 77
1 117
2 108
3 116
4 105
5 112
6 108
7 101
8 32
9 108
10 105
11 110
12 101
13 10
14 9
15 32
16 115
17 116
18 114
19 105
20 110
21 103
22 32
23 108
24 105
25 116
26 101
27 114
28 97
29 108
 0  0x4d
 1  0x75
 2  0x6c
 3  0x74
 4  0x69
 5  0x70
 6  0x6c
 7  0x65
 8  0x20
 9  0x6c
 10  0x69
 11  0x6e
 12  0x65
 13  0xa
 14  0x9
 15  0x20
 16  0x73
 17  0x74
 18  0x72
 19  0x69
 20  0x6e
 21  0x67
 22  0x20
 23  0x6c
 24  0x69
 25  0x74
 26  0x65
 27  0x72
 28  0x61
 29  0x6c

*/
