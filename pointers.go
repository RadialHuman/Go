package main

import "fmt" // always double quotes

func main() {
	x := 15 //less verbose than ''var count int''
	a := &x // assigns memory location
	b := x
	fmt.Println(x, a, b, *a) //"*" is used to read value att the location
	*a = 5                   //changes the value of x
	fmt.Println(x)
	*a = *a * *a
	fmt.Println(x, *a)
}
