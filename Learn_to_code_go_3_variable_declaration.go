// https://www.youtube.com/playlist?list=PLSak_q1UXfPp971Hgv7wHCU2gDOb13gBQ

package main

import (
	"fmt"
)

var x = 32 // this can go on to the main func and for the whole package, this is package level declaration

// := is always inside code block for initilization

func main() {
	// scope : where variable can live, within code block
	fmt.Println(x, "main")
}

func foo() {
	fmt.Println(x, "func")
}
