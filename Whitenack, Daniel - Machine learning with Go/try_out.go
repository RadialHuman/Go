package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {

	// opening the csv
	f, err := os.Open("E:/etc/iris.csv")
	if err != nil {
		log.Fatal(err)
	}

	// reading the csv
	r := csv.NewReader(f)
	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	// finding the biggest in a column
	var intMax float64
	for _, record := range records[1:] { // _ is the index, [1:] is to remove the headers
		intVal, err := strconv.ParseFloat(record[0], 64) // the first column
		if err != nil {
			log.Fatal(err)
		}
		if intVal > intMax {
			intMax = intVal
		}
	}
	fmt.Println(intMax)
}
