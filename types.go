package main

import (
	"fmt"
)

// func add(a float64, b float64) float64 { //return type is also specified
func add(a, b float64) float64 { // this is condensed way of writing if both the tyeps are the same
	return a + b
}

func main() {
	//var num1 float64 = 5.6
	//var num2 float64 = 6.4
	var num1, num2 float64 = 5.6, 6.4 // reduced way of declaring
	fmt.Println(add(num1, num2))

	// type conversion
	var a int = 10
	var b float64 = float64(a)
	x := a
	fmt.Println(a, b, x)
}

/*type cannot be changed
same float value should be used cant exchange between 64 and 32
float64 is default
var num1 :=5.6 assigns float64
constants are const x int = 3.14*/
