// https://www.youtube.com/playlist?list=PLSak_q1UXfPp971Hgv7wHCU2gDOb13gBQ

package main

import (
	"fmt"
)

func main() {
	x := 4 // this is to assign value, symbolizing, gopher or punisher
	// provides ease of programming and write cleaner code
	// can be used inside a code block
	// needs to be used or else error
	// this is initialization of variable
	y := "Something"
	fmt.Println(x*3, y)

	// statement is made of expression

	// to assign another value to existing variable
	x = 50
	fmt.Println(x)
}
