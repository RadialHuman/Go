package main

import "fmt"

// structs are like classes used as functions in go
// this is to make a game where car is being controlled 

type car struct {
	gas_peddle uint16  //0-65535 unsigned int
	brake_peddle uint16
	sterring_wheel int16 //-32k -32k
	top_speed_kmh float64
}

func main(){
	a_car := car{22341, 0, 12561, 225.0} // create a car with these parameters
	fmt.Println(a_car.gas_peddle)

}