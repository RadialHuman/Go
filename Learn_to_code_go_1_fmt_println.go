// https://www.youtube.com/playlist?list=PLSak_q1UXfPp971Hgv7wHCU2gDOb13gBQ

package main // main of the program

import ( // imports pagages from std lib or 3rd party
	"fmt" // called - fumt , can be found in godoc.org
	// these are not public or private but exporeted or not exported.
	// the packages ike Println are visible from outside (capital letter)
)

func main() { // program runs here, launchhes other process, entry and exit  point to the program
	// golang specs, keywords, for package, func definitions
	fmt.Println("Who is this?")

	// in the println syntax, it returns two parameters
	// print has input parameter as interface , which is a datatype and can input as may as needed
	n, err := fmt.Println("Who is this?")
	fmt.Println(n, err) // n is the number of bytes written, gets caught by this

	// these can be thrown into abyss:
	_, _ = fmt.Println(n, err)

	// go is pollution free, dont declare variables if not gonna use it
}
