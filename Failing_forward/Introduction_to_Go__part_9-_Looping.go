package main

import (
	"fmt"
)

func main() {
	for i := 0; i < 5; i++ {
		fmt.Println("a")
		fmt.Println("\n")
	}

	for i := 0; i < 10; i = i + 2 {
		fmt.Println(i)
		fmt.Println("\n")
	}

	for i, j := 0, 0; i < 10; i, j = i+1, j+1 { // increment operation is nto an exp, so cant be used
		fmt.Println(i, j)
		fmt.Println("\n")
	}

	x := 0 // x is scoped to the main and not the loop anymore
	for ; x < 10; x++ {
		fmt.Println("This works")
	}

	// no while or do as its just
	// break or continue can be sued to make a for while or do
	// Lable can be defined with : and break can go to the lable to break it
	// as break can be applied to the nearest loop and the outer one will still go running

	// slice looping
	// syntax 1
	s := []int{1, 2, 3, 4, 5}
	for i := 0; i < len(s); i++ {
		fmt.Println(s[i])
	}
	//syntax 2
	s = []int{5, 4, 3, 2, 1}
	for k, v := range s {
		fmt.Println(v) // value
		fmt.Println(k) // index
	}
	// same can be done for maps

	d := "Some thing"
	for k, v := range d {
		fmt.Println(k, v, string(v))
	}

	// channels can also br for looped through, later
}

/*
a


a


a


a


a


0


2


4


6


8


0 0


1 1


2 2


3 3


4 4


5 5


6 6


7 7


8 8


9 9


This works
This works
This works
This works
This works
This works
This works
This works
This works
This works
1
2
3
4
5
5
0
4
1
3
2
2
3
1
4
0 83 S
1 111 o
2 109 m
3 101 e
4 32
5 116 t
6 104 h
7 105 i
8 110 n
9 103 g

*/
