/*
Collection types

*/

package main

import (
	"fmt"
)

func main() {
	// ARRAYS form basis of slices
	a := [3]int{23, 52, 12}
	fmt.Println(a, a[1])
	b := [...]string{"something", "is", "here"} // ... means make it as large as the elements passed
	fmt.Println(b)
	var c [4]float32 // creation of empty array
	fmt.Println(c)
	c[2] = 3.14 // updating an array
	fmt.Println("updated array is now", c)
	fmt.Println(len(c)) // length of the array
	// arrays are of homogeneous type

	// array of arrays
	var matrix [3][3]int = [3][3]int{[...]int{1, 0, 0}, [...]int{0, 1, 0}, [...]int{0, 0, 1}}
	fmt.Println(len(matrix), matrix)

	// ARRAYS are values and not pointers which point to a value
	// so when copied it creates literal value and doesnot point to something else
	d := a
	fmt.Println(d)
	d[2] = 0
	fmt.Println(a) // is not affected
	fmt.Println(d)

	// if this is to be avoided, d can be made to point at the address of a
	e := &a
	fmt.Println(e)
	e[2] = 0
	fmt.Println(a) // will be affected
	fmt.Println(e)

	// Since arrays are of fixed size, is nto that useful
	// Slice on the other hand is not rigid
	f := []int{1, 2, 3}
	fmt.Println(f, f[1])

	fmt.Printf("Length: %v\n", len(f))
	fmt.Printf("Capacity: %v\n", cap(f)) // length of the slcie may be less than or equal to its total capacity

	// slcies are reference type so they are always pointing to the other slice they are copying
	g := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	h := g[:]
	i := g[3:]
	j := g[3:6]
	k := g[:6]
	g[9] = 10
	fmt.Println(h)
	fmt.Println(i)
	fmt.Println(j)
	fmt.Println(k)

	// make fucntion can be used to create slices, this is better for bigger data, to avoid copying and creating a different slice
	l := make([]int, 3)
	fmt.Println(l, l[1])
	fmt.Printf("Length: %v\n", len(l))
	fmt.Printf("Capacity: %v\n", cap(l))

	m := make([]int, 3, 10) // the thrid parameter sets the capacity
	fmt.Println(m, m[1])
	fmt.Printf("Length: %v\n", len(m))
	fmt.Printf("Capacity: %v\n", cap(m))
	m[2] = 10 // altering a slice can be within the length but not for the capacity
	fmt.Println(m)

	m = append(m, 10)
	fmt.Println(m)
	fmt.Printf("Length: %v\n", len(m))
	fmt.Printf("Capacity: %v\n", cap(m))

	m = append(m, 10, 20, 30, 40, 50) // variatic function, all after first argument is considered to be one parameter
	fmt.Println(m)
	fmt.Printf("Length: %v\n", len(m))
	fmt.Printf("Capacity: %v\n", cap(m))

	// if the capacity is not intialized, the capacity is fixed by go automatically

	// concatenate slices
	n := []int{1, 2, 3}
	o := []int{4, 5, 6}
	// fmt.Println(append(n, o)) // wont help as the secnd one should be int and nto a slice
	fmt.Println(append(n, o...)) // in JS its a spread operator where the apssed argument is sent as its values

	// removing elements from slices from the end
	p := append(n, o...)[:len(append(n, o...))-1]
	fmt.Println(p)

	// removing elements from slices from the front
	q := p[1:]
	fmt.Println(q)

	// removing elements from slices from the middle
	fmt.Println(p)
	r := append(p[:1], p[3:]...) // this causes problem as it alters the underlying array
	fmt.Println(r)
	fmt.Println(p) // this is the altered p which can be problamatic, loop can be sued to avoid this

}

/*
[23 52 12] 52
[something is here]
[0 0 0 0]
updated array is now [0 0 3.14 0]
4
3 [[1 0 0] [0 1 0] [0 0 1]]
[23 52 12]
[23 52 12]
[23 52 0]
&[23 52 12]
[23 52 0]
&[23 52 0]
[1 2 3] 2
Length: 3
Capacity: 3
[1 2 3 4 5 6 7 8 9 10]
[4 5 6 7 8 9 10]
[4 5 6]
[1 2 3 4 5 6]
[0 0 0] 0
Length: 3
Capacity: 3
[0 0 0] 0
Length: 3
Capacity: 10
[0 0 10]
[0 0 10 10]
Length: 4
Capacity: 10
[0 0 10 10 10 20 30 40 50]
Length: 9
Capacity: 10
[1 2 3 4 5 6]
[1 2 3 4 5]
[2 3 4 5]
[1 2 3 4 5]
[1 4 5]
[1 4 5 4 5]
*/
