package main

import (
	"fmt"
)

func main() {
	a := "something"
	b := a // makes a copy and not the same thing with pointer
	fmt.Println(a, b)
	a = "nothing"
	fmt.Println(a, b)

	var c string = "something"
	var d *string = &c                // address of c wil be stored in d
	fmt.Printf("%T,%T,%v\n", c, d, d) // d is a pointer with pointing to a string
	fmt.Println(c, *d)
	*d = "one thing" // dereferencing can also be used to change the value
	fmt.Println(c, *d)

	// pointers in arrays
	x := [...]int{1, 2, 3}
	e := &x[0]
	f := &x[1]
	fmt.Printf("%v %p %v %p %v \n", x, e, *e, f, *f) // pointer vales and location for array elements

	// go does not permit pointer operations using int (pointer arithmetic)
	// zero value of pointe is <nil>

	// slcie is a projection of underlying array so anychange in aunderlying arrayw ill cange the slice value

}

/*
something something
nothing something
string,*string,0xc000036210
something something
one thing one thing
[1 2 3] 0xc00000a3a0 1 0xc00000a3a8 2
*/
