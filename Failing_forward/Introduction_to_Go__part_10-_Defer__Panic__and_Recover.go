package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	fmt.Println("1")
	defer fmt.Println("2") // this will be executed only after the main is done returning
	fmt.Println("3")

	defer fmt.Println("a")
	defer fmt.Println("b")
	defer fmt.Println("c") // if all have defer then the last one defered will be executed first
	// because of LIFO order , often defer is used to close resources, which are suppsoed to be closed in the oppsootive way tis opened

	res, err := http.Get("http://www.google.com/robots.txt")
	if err != nil {
		log.Fatal(err)
	}
	robots, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close() // applciation of defer to free up resources and close the app
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%T\n", robots)

	x := "start"
	defer fmt.Println(x) // even though, defer runs after main, the value is taken which is at the time of defer
	x = "end"

	// Panicking
	// errors are considered normal value, so panicking is used when app cant go further

	// _, err2 := http.Get("http://www.google.com/robots.txt")
	// if err2 != nil {
	// 	panic(.Error())
	// }

	// Main function > defer > panic > return value <- order of execution

	// CUSTOME fucntion
	fmt.Println("starting again")
	defer func() {
		if err := recover(); err != nil {
			log.Println("Error :", err)
		}
	}() // defer takes a fucntion call
	panic("Something went down")
	fmt.Println("ending")
}

/*
1
3
[]uint8
starting again
2018/10/10 15:48:31 Error : Something went down
start
c
b
a
2
*/
